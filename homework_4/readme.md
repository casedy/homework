## Info

Main idea is to make common entity as a parent. And implement all methods and properties of the parent in each children. 
So we are able to use any of child everywhere.
The reason is - we maintain substitution principle.
